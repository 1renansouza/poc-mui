import React from 'react'

import {
  Container,
  AppBar,
  Button,
  Divider,
  List,
  ListItemIcon,
  ListItemText,
  ListItem,
  IconButton,
  Menu,
  MenuItem,
  Avatar,
  Toolbar,
  Drawer,
} from '@material-ui/core'

import {
  Inbox as InboxIcon,
  Mail as MailIcon,
  Menu as MenuIcon,
  LocationOn as LocationOnIcon,
  ChevronLeft as ChevronLeftIcon,
} from '@material-ui/icons'

import { makeStyles, useTheme } from '@material-ui/core/styles'
import clsx from 'clsx'

import { Logo } from './components'

const appBarStyles = makeStyles(theme => ({
  appBar: {
    padding: '15px 0',
    marginBottom: theme.spacing(2),
  },
  logo: {
    flexGrow: 1,
  },
  siteButton: {
    textTransform: 'none',
  },
  toolbar: {
    padding: 0,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  profileMenu: {
    boxShadow: theme.shadows[0],
  },
  sideMenu: {
    width: 250,
  },
}))

export default function MenuAppBar() {
  const theme = useTheme()
  const classes = appBarStyles()

  /* Profile Menu */
  const profileMenuAnchor = React.useRef(null)
  const [openProfileMenu, setOpenProfileMenu] = React.useState(false)
  const handleToggleProfileMenu = () => {
    setOpenProfileMenu(openProfileMenu => !openProfileMenu)
  }

  const closeProfileMenu = () => {
    setOpenProfileMenu(false)
  }


  /* Drawer Menu */
  const [openSideMenu, setOpenSideMenu] = React.useState(false)
  const handleToggleSideMenu = () => {
    setOpenSideMenu(openSideMenu => !openSideMenu)
  }

  const closeSideMenu = () => {
    setOpenSideMenu(false)
  }

  return (
    <>
      <AppBar className={classes.appBar} position='static'>
        <Container>
          <Toolbar className={classes.toolbar}>
            <IconButton
              edge='start'
              color='secondary'
              onClick={handleToggleSideMenu}
              className={classes.menuButton}
            >
              <MenuIcon />
            </IconButton>
            
            <div className={classes.logo}>
              <Logo />
            </div>

            <Button
              className={clsx(classes.siteButton, classes.menuButton)}
              color='secondary'
              startIcon={<LocationOnIcon />}
            >
              GSA Kavanagh Court
            </Button>

            <>
              <IconButton ref={profileMenuAnchor} onClick={handleToggleProfileMenu}>
                <Avatar alt={theme.user.name} src={theme.user.avatar} />
              </IconButton>

              <Menu
                anchorEl={profileMenuAnchor.current}
                keepMounted
                elevation={1}
                open={openProfileMenu}
                onClose={closeProfileMenu}
              >
                <MenuItem onClick={closeProfileMenu}>Profile</MenuItem>
                <MenuItem onClick={closeProfileMenu}>Logout</MenuItem>
              </Menu>
            </>
          </Toolbar>
        </Container>
      </AppBar>

      <Drawer open={openSideMenu}>
        <Button color='primary' startIcon={<ChevronLeftIcon />} onClick={closeSideMenu}>
          Close Menu
        </Button>
        <Divider />
        <List className={classes.sideMenu}>
          {['This', 'is', 'not', 'implemented'].map((text, index) => (
            <ListItem button key={text}>
              <ListItemIcon>{index % 2 === 0 ? <InboxIcon /> : <MailIcon />}</ListItemIcon>
              <ListItemText primary={text} />
            </ListItem>
          ))}
        </List>
      </Drawer>
    </>
  )
}