  
import React from 'react'
import theme from '../../../../theme'


const Logo = () => {
  return (
    <img alt={theme.logo.alt} src={theme.logo.src} />
  )
}

export default Logo
