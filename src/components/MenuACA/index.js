import React from 'react'
import clsx from 'clsx'

import {
  Grid,
  Container,
  AppBar,
  Button,
  Menu,
  MenuItem,
  Toolbar,
  Typography,
} from '@material-ui/core'

import {
  ExpandMore as ExpandMoreIcon,
  LocationOn as LocationOnIcon,
} from '@material-ui/icons'

import { makeStyles } from '@material-ui/core/styles'

import { Logo, SubMenu } from './components'

const useStyles = makeStyles(theme => ({
  wrapper: {
    marginBottom: theme.spacing(7),
  },
  appBar: {
    padding: '15px 0',
    marginBottom: theme.spacing(3),
  },
  logo: {
    flexGrow: 1,
  },
  menuButton: {
    textTransform: 'none',
    color: theme.palette.grey[100],
    '&:hover': {
      backgroundColor: 'rgba(255, 255, 255, 0.07)',
    },
  },
  toolbar: {
    padding: 0,
  },
  menuItem: {
    fontSize: '13px',
    textTransform: 'uppercase',
    fontWeight: 'bold',
    opacity: 0.5,
    borderBottom: '3px solid',
    transition: 'all 400ms',
    cursor: 'pointer',
    borderBottomColor: 'transparent',
    '&:hover, &.active': {
      opacity: 1,
      borderBottomColor: 'white',
    },
  },
}))

export default function MenuAppBar({ back }) {
  const classes = useStyles()

  /* Profile Menu */
  const profileButton = React.useRef(null)
  const [openProfileMenu, setOpenProfileMenu] = React.useState(false)
  const handleToggleProfileMenu = () => {
    setOpenProfileMenu(openProfileMenu => !openProfileMenu)
  }

  const closeProfileMenu = () => {
    setOpenProfileMenu(false)
  }

  return (
    <div className={classes.wrapper}>
      <AppBar className={classes.appBar} position='static'>
        <Container>
          <Toolbar className={classes.toolbar}>
            <div className={classes.logo}>
              <Logo />
            </div>

            <Button
              className={classes.menuButton}
              ref={profileButton}
              onClick={handleToggleProfileMenu}
              startIcon={<ExpandMoreIcon color='error' />}
            >
              Hi Mike Wazowski
            </Button>

            <Menu
              anchorEl={profileButton.current}
              keepMounted
              elevation={1}
              open={openProfileMenu}
              onClose={closeProfileMenu}
            >
              <MenuItem onClick={closeProfileMenu}>Profile</MenuItem>
              <MenuItem onClick={closeProfileMenu}>Logout</MenuItem>
            </Menu>
          </Toolbar>
          <Grid container alignItems='baseline' spacing={2}>
            <Grid item>
              <Button
                className={classes.menuButton}
                color='secondary'
                startIcon={<LocationOnIcon color='error' />}
              >
                Singelstaete, Amsterdam
              </Button>
            </Grid>
            <Grid item>
              <Typography className={classes.menuItem}>
                Dashboard
              </Typography>
            </Grid>
            <Grid item>
              <Typography className={clsx(classes.menuItem, 'active')}>
                Inventory
              </Typography>
            </Grid>
            <Grid item>
              <Typography className={classes.menuItem}>
                Hardware
              </Typography>
            </Grid>
            <Grid item>
              <Typography className={classes.menuItem}>
                Companies
              </Typography>
            </Grid>
            <Grid item>
              <Typography className={classes.menuItem}>
                Users
              </Typography>
            </Grid>
            <Grid item>
              <Typography className={classes.menuItem}>
                Access
              </Typography>
            </Grid>
            <Grid item>
              <Typography className={classes.menuItem}>
                Events
              </Typography>
            </Grid>
            <Grid item>
              <Typography className={classes.menuItem}>
                Data Refresh
              </Typography>
            </Grid>
          </Grid>
        </Container>
      </AppBar>
      <SubMenu back={back} />
    </div>
  )
}