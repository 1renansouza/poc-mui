  
import React from 'react'
import { makeStyles } from '@material-ui/core/styles'

import {
  Typography,
} from '@material-ui/core'

const useStyles = makeStyles(theme => ({
  title: {
    marginRight: theme.spacing(1),
  },
  quote: {
    color: theme.palette.grey[400],
    letterSpacing: 1,
  },
}))
const Logo = () => {
  const classes = useStyles()

  return (
    <>
     <Typography variant='h1' component='span' className={classes.title}>
       Access Control
     </Typography>
     <Typography variant='caption' component='span' className={classes.quote}>
       BY SALTO
     </Typography>
    </>
  )
}

export default Logo
