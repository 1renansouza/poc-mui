import React from 'react'

import {
  Grid,
  Container,
  Button,
  Typography,
} from '@material-ui/core'

import {
  ArrowLeft as ArrowLeftIcon,
} from '@material-ui/icons'

import { makeStyles } from '@material-ui/core/styles'
import clsx from 'clsx'

const useStyles = makeStyles(theme => ({
  wrapper: {
    boxShadow: theme.shadows[1],
  },
  menuItem: {
    fontSize: '13px',
    textTransform: 'uppercase',
    fontWeight: 'bold',
    opacity: 0.75,
    borderBottom: '2px solid',
    paddingBottom: '7px',
    marginBottom: '-8px',
    transition: 'all 400ms',
    cursor: 'pointer',
    borderBottomColor: 'transparent',
    '&:hover, &.active': {
      opacity: 1,
      borderBottomColor: theme.palette.primary.main,
    },
  },
  menuItemButton: {
    fontSize: '13px',
    textTransform: 'uppercase',
    fontWeight: 'bold',
    opacity: 0.75,
    marginTop: '-16px',
    transition: 'all 400ms',
  }
}))

const SubMenu = ({ back }) => {
  const classes = useStyles()
  

  return (
    <div className={classes.wrapper}>
      <Container>
        <Grid container alignItems='baseline' spacing={2}>
        {
          back 
          ? (
            <>
            <Grid item>
                <Button className={classes.menuItemButton} startIcon={<ArrowLeftIcon />}>
                  Back to Users Overview
                </Button>
              </Grid>
            </>
          )
          : (
            <>
              <Grid item>
                <Typography className={clsx(classes.menuItem, 'active')}>
                  Offices
                </Typography>
              </Grid>
              <Grid item>
                <Typography className={classes.menuItem}>
                  Spaces
                </Typography>
              </Grid>
              <Grid item>
                <Typography className={classes.menuItem}>
                  Zones
                </Typography>
              </Grid>
            </>
          )
        }
        </Grid>
      </Container>
    </div>
  )
}

export default SubMenu
