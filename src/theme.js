import { red } from '@material-ui/core/colors'
import { createMuiTheme } from '@material-ui/core/styles'

import logo from './resources/images/logo-ks.png'
import avatar from './resources/images/avatar.jpg'

// A custom theme for this app
const themeaca = createMuiTheme({
  palette: {
    primary: {
      main: '#333639',
    },
    secondary: {
      main: '#51bbb4',
    },
    error: {
      main: red.A400,
    },
    background: {
      default: '#ffffff',
    },
    grey: {
      300: '#e0e0e0',
    },
  },
  typography: {
    h1: {
      fontSize: '21px',
      fontWeight: 'bolder',
    },
    caption: {
      fontSize: '11px',
    },
    subtitle2: {
      fontWeight: 'bolder',
    },
  },
  user: {
    name: 'Renan Souza',
    avatar: avatar,
  },
  title: 'SALTO Keys as a Service',
  shadows: [
    'none',
    '0px 5px 10px -5px rgba(0, 0, 0, 0.3)',
    '0px 5px 10px -5px rgba(0, 0, 0, 0.5)',
    '0px 5px 15px 0px rgba(0, 0, 0, 0.2)',
    'none',
    'none',
    'none',
    'none',
    '0px 5px 10px -5px rgba(0, 0, 0, 0.5)',
  ].concat(Array(16).fill('none')),
})


// A custom theme for this app
const themeks = createMuiTheme({
  palette: {
    primary: {
      main: '#0188cf',
    },
    secondary: {
      main: '#ffffff',
    },
    error: {
      main: red.A400,
    },
    background: {
      default: '#f7f7f7',
    },
  },
  typography: {
    subtitle2: {
      fontWeight: 'bolder',
    },
  },
  logo: {
    src: logo,
    alt: 'SALTO Keys as a Service Logo',
  },
  user: {
    name: 'Renan Souza',
    avatar: avatar,
  },
  title: 'SALTO Keys as a Service',
  shadows: [
    'none',
    '0px 5px 10px -5px rgba(0, 0, 0, 0.3)',
    '0px 5px 10px -5px rgba(0, 0, 0, 0.5)',
    '0px 5px 15px 0px rgba(0, 0, 0, 0.2)',
    'none',
    'none',
    'none',
    'none',
    '0px 5px 10px -5px rgba(0, 0, 0, 0.5)',
  ].concat(Array(16).fill('none')),
  props: {
    // Name of the component
    MuiButtonBase: {
      // The properties to apply
      disableRipple: true,
    },
  },
})

export default themeaca
