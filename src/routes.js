import React from 'react'
import { Route, Switch, Redirect } from 'react-router-dom'

import {
	AccessGroupsContainer,
	ACAContainer,
	ACAUserContainer
} from './pages'

export default () => (
	<Switch>
		<Route path='/access-groups' exact component={AccessGroupsContainer} />
		<Route path='/aca' exact component={ACAContainer} />
		<Route path='/' exact component={ACAUserContainer} />
		<Redirect from='*' to='/' />
	</Switch>
)
