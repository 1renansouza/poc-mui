import React from 'react'
import ReactDOM from 'react-dom'

import { withStyles } from '@material-ui/core/styles'

import { ThemeProvider } from '@material-ui/core/styles'
import { Router } from 'react-router-dom'
import { SnackbarProvider } from 'notistack'

import HTML5Backend from 'react-dnd-html5-backend'
import { DndProvider } from 'react-dnd'

import CssBaseline from '@material-ui/core/CssBaseline'

import * as serviceWorker from './serviceWorker'
import Routes from './routes.js'
import history from './history'

import theme from './theme'

const GlobalCss = withStyles({
  // @global is handled by jss-plugin-global.
  '@global': {
    'body': {
      minWidth: '1161px',
    },
    '.MuiSvgIcon-root.MuiSelect-icon.MuiSelect-iconOutlined': {
      color: theme.palette.secondary.main,
    },
  },
})(() => null);

const App = () => (
  <div className='app'>
  <DndProvider backend={HTML5Backend}>
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <GlobalCss />
      <Router history={history}>
        <SnackbarProvider maxSnack={3}>
          <Routes />
        </SnackbarProvider>
      </Router>
    </ThemeProvider>
  </DndProvider>
  </div>
)

ReactDOM.render(<App />, document.getElementById('root'))

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()