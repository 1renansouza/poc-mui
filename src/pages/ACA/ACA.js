  
import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import clsx from 'clsx'

import {
  Container,
  Grid,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableSortLabel,
  Typography,
  TableBody,
  FormControl,
  Select,
  MenuItem,
} from '@material-ui/core'

import {
  ArrowDropDown as ArrowDropDownIcon,
  ArrowLeft as ArrowLeftIcon,
  ArrowRight as ArrowRightIcon,
} from '@material-ui/icons'

import PageHeader from './PageHeader'

const useStyles = makeStyles(theme => ({
  textButton: {
    fontWeight: 'bolder',
    '& + &': {
      marginLeft: theme.spacing(1),
    },
  },
  button: {
    textTransform: 'none',
    borderRadius: 0,
    marginRight: theme.spacing(1),
  },
  searchInput: {
    border: '1px solid rgba(0, 0, 0, 0.23)',
    padding: `0 ${theme.spacing(2)}px`,
    fontSize: `0.875rem`,
    height: '36px',
    verticalAlign: 'middle',
    marginTop: '1px',
  },
  buttonContained: {
    boxShadow: theme.shadows[0],
    borderRadius: 0,
    color: 'white',
    fontSize: '11px',
    fontWeight: 'bolder',
    verticalAlign: 'bottom',
    height: '36px',
  },
  popoverContent: {
    padding: `${theme.spacing(1)}px ${theme.spacing(2)}px`,
  },
  alignRight: {
    textAlign: 'right',
  },
  divider: {
    margin: `${theme.spacing(1)}px 0`,
  },
  tableWrapper: {
    marginTop: theme.spacing(3),
  },
  tableHeader: {
    textTransform: 'uppercase',
    fontSize: '11px',
    color: theme.palette.grey[600],
  },
  tableRow: {
    border: '1px solid rgba(224, 224, 224, 1)',
  },
  tableRowInner: {
    border: '1px solid rgba(224, 224, 224, 1)',
  },
  tableCell: {
    padding: '10px 16px',
    position: 'relative',
  },
  tableCellNoPadding: {
    padding: 0,
    position: 'relative',
  },
  officeNamePlaceholder: {
    opacity: 0,
    pointerEvents: 'none',
    fontWeight: 'bolder',
    maxWidth: '20vw',
  },
  officeName: {
    color: theme.palette.secondary.main,
    transition: 'max-width 400ms',
    fontWeight: 'bolder',
    padding: '10px 16px',
    boxSizing: 'border-box',
    background: theme.palette.background.default,
    position: 'absolute',
    marginTop: '-10px',
    maxWidth: '20vw',
    overflow: 'hidden',
    whiteSpace: 'nowrap',
    zIndex: '2',
    '&:hover': {
      maxWidth: '100vw',
    },
  },
  tablePagination: {
    marginTop: theme.spacing(2),
  },
  paginationSelect: {
    paddingRight: theme.spacing(1),
    marginRight: theme.spacing(2),
  },
  pageText: {
    padding: `3px ${theme.spacing(1)}px`,
    cursor: 'pointer',
    transition: 'all 400ms',
    fontWeight: 'bolder',
    opacity: '0.7',
    border: '1px solid transparent',
    borderRadius: '4px',
    '&:hover, &.active': {
      opacity: 1,
      borderColor: theme.palette.primary.main,
    },
    '& + &': {
      marginLeft: theme.spacing(1),
    },
  },
  paginationIcon: {
    verticalAlign: 'middle',
    opacity: '0.75',
    '&:hover': {
      opacity: 1,
    },
  },
}))

const headers = ['Office Name', 'Location', 'Type', 'Id', 'Status']

const ACA = () => {
  const classes = useStyles()
  return (
    <Container>
      <Grid container>
        <Grid item xs={8}>
          <PageHeader classes={classes} />
        </Grid>
        <Grid item xs={8}>
          <Table className={classes.tableWrapper}>
            <TableHead>
              <TableRow className={classes.tableRow}>
                {
                  headers.map((item, index) => (
                    <TableCell className={classes.tableCell} key={index}>
                      <TableSortLabel active direction={index % 2 === 0 ? 'asc' : 'desc'} IconComponent={ArrowDropDownIcon}>
                        <Typography className={classes.tableHeader} variant='subtitle2'>
                          {item}
                        </Typography>
                      </TableSortLabel>
                    </TableCell>
                  ))
                }
              </TableRow>
            </TableHead>
            <TableBody>
              {
                Array(10).fill(null).map((item, index) => (
                  <TableRow className={classes.tableRow} key={index}>
                    <TableCell className={classes.tableCellNoPadding}>
                      <Typography noWrap variant='body2' className={classes.officeName}>
                        Hello Office Ultra Long Name How To Handle Hello Office Ultra Long Name How To Handle
                      </Typography>
                      <Typography noWrap variant='body2' className={classes.officeNamePlaceholder}>
                        Hello Office Ultra Long Name How To Handle Hello Office Ultra Long Name How To Handle
                      </Typography>
                    </TableCell>
                    <TableCell className={classes.tableCell}>
                      <Typography variant='body2'>
                        1st Floor
                      </Typography>
                    </TableCell>
                    <TableCell className={classes.tableCell}>
                      <Typography variant='body2'>
                        Long Term Office
                      </Typography>
                    </TableCell>
                    <TableCell className={classes.tableCell}>
                      <Typography variant='body2'>
                        2458275
                      </Typography>
                    </TableCell>
                    <TableCell className={classes.tableCell}>
                      <Typography variant='body2'>
                        Locked
                      </Typography>
                    </TableCell>
                  </TableRow>
                ))
              }
            </TableBody>
          </Table>
          <CustomTablePagination classes={classes} />
        </Grid>
      </Grid>
    </Container>
  )
}

const CustomTablePagination = ({ classes }) => {

  return (
    <Grid container alignItems='center' className={classes.tablePagination}>
      <Grid item>
        <FormControl variant='outlined' size='small'>
          <Select value={10} onChange={() => {}} className={classes.paginationSelect}>
            <MenuItem value={10}>show 10</MenuItem>
            <MenuItem value={20}>show 20</MenuItem>
            <MenuItem value={30}>show 30</MenuItem>
          </Select>
        </FormControl>
      </Grid>
      <Grid item>
        <ArrowLeftIcon className={classes.paginationIcon} />
      </Grid>
      <Grid item>
        {
          [1, 2, 3, 4, 5, 6, 7, 8].map(item => (
            <Typography
              variant='body2'
              component='span'
              key={item}
              className={clsx(classes.pageText, {active: item === 1})}
            >
              {item}
            </Typography>
          ))
        }
      </Grid>
      <Grid item>
        <ArrowRightIcon className={classes.paginationIcon} />
      </Grid>
    </Grid>
  );
}

export default ACA
