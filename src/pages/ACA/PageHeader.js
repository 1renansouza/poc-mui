import React, { useRef, useState } from 'react'

import {
  InputBase,
  Grid,
  Button,
  Typography,
  Popover,
  FormControlLabel,
  Checkbox,
  Divider,
} from '@material-ui/core'

import {
  Refresh as RefreshIcon,
  Tune as TuneIcon,
} from '@material-ui/icons'

const PageHeader = ({ classes }) => {
  const [openFilter, setOpenFilter] = useState(false)
  const filterButton = useRef(null)

  const handleFilterClick = () => {
    setOpenFilter(true)
  }

  const closeFilter = () => {
    setOpenFilter(false)
  }

  return (
    <Grid container alignItems='center'>
      <Grid item xs={6}>
        <Grid container>
          <Grid item xs={12}>
            <Typography variant='subtitle2'>
              Offices are bookable rooms.
            </Typography>
          </Grid>
          <Grid item xs={12}>
            <Button
              className={classes.textButton}
              size='small'
              color='secondary'
              startIcon={<RefreshIcon />}
            >
              Refresh the list
            </Button>
            <Button
              className={classes.textButton}
              size='small'
              color='secondary'
            >
              Edit Hardware
            </Button>
          </Grid>
        </Grid>
      </Grid>
      <Grid item xs={6} className={classes.alignRight}>
        <Button
          className={classes.button}
          variant='outlined'
          onClick={handleFilterClick}
          ref={filterButton}
          endIcon={<TuneIcon />}
        >
          Filters
        </Button>

        <InputBase
          className={classes.searchInput}
          placeholder='Search for offices'
        />
        <Button
          className={classes.buttonContained}
          variant='contained'
          color='secondary'
        >
          Search
        </Button>

        <Popover
          elevation={3}
          onClose={closeFilter}
          open={openFilter}
          anchorEl={filterButton.current}
          className={classes.wrapper}
          anchorOrigin={{ vertical: 'bottom', horizontal: 'center', }}
          transformOrigin={{ vertical: 'top', horizontal: 'center', }}
        >
          <div className={classes.popoverContent}>
            <Grid container>
              <Grid item xs={12}>
                <FormControlLabel
                  control={
                    <Checkbox />
                  }
                  label='With E-locks'
                />
              </Grid>
              <Grid item xs={12}>
                <FormControlLabel
                  control={
                    <Checkbox />
                  }
                  label='Without E-locks'
                />
              </Grid>
            </Grid>
            <Divider className={classes.divider} />
            <div className={classes.alignRight}>
              <Button className={classes.textButton}>Cancel</Button>
              <Button className={classes.textButton} color='secondary'>Apply</Button>
            </div>
          </div>
        </Popover>
      </Grid>
    </Grid>
  )
}

export default PageHeader
