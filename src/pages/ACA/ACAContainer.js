  
import React from 'react'
import ACA from './ACA'

import Menu from '../../components/MenuACA'

const ACAContainer = () => {
  return (
    <>
      <Menu />
      <ACA />
    </>
  )
}

export default ACAContainer
