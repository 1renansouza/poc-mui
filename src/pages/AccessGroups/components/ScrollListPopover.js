  
import React, { useState } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import clsx from 'clsx'

import {
  Popover,
  List,
  ListItem,
  ListItemText,
  ListSubheader,
  TextField,
  Typography,
} from '@material-ui/core'

import {
  Skeleton,
} from '@material-ui/lab'

import {
  Search as SearchIcon,
} from '@material-ui/icons'

const useStyles = makeStyles(theme => ({
  wrapper: {
    pointerEvents: 'none',
  },
  content: {
    pointerEvents: 'auto',
  },
  textField: {
    marginTop: theme.spacing(1),
    transition: 'box-shadow 500ms',
    boxShadow: theme.shadows[0],
  },
  withBoxShadow: {
    boxShadow: theme.shadows[3],
  },
  noPadding: {
    padding: 0,
  },
  list: {
    height: 220,
    width: 210,
    overflow: 'scroll',
  },
}))

const mockList = Array(25).fill({ name: 'Mock' })

const ScrollListPopover = ({ open, anchorEl }) => {
  const [loading, setLoading] = useState(false)
  const classes = useStyles()

  const toggleLoading = (event) => {
    event.preventDefault()
    event.stopPropagation()
    setLoading(!loading)
  }

  return (
    <Popover
      elevation={3}
      open={open}
      anchorEl={anchorEl}
      className={classes.wrapper}
      anchorOrigin={{ vertical: 'bottom', horizontal: 'center', }}
      transformOrigin={{ vertical: 'top', horizontal: 'center', }}
    >
      <div className={classes.content}>
        <List className={classes.noPadding} dense onClick={toggleLoading}>
          <ListSubheader>
            <TextField
              fullWidth
              label='Search'
              className={classes.textField}
              margin='dense'
              InputProps={{
                endAdornment: <SearchIcon fontSize='small' color='primary' />
              }}
            />
          </ListSubheader>
          <li>
            <ul className={clsx(classes.noPadding, classes.list)}>
              { 
                mockList.map((item, index) => (
                  <ListItem key={index}>
                    <ListItemText primary={
                      loading
                        ? <Skeleton height={10} width="100%" />
                        : (
                          <Typography title={item.name} noWrap variant='body2'>
                            {item.name}
                          </Typography>
                        )
                    } />
                  </ListItem>
                ))
              }
            </ul>
          </li>
        </List>
      </div>
    </Popover>
  )
}

export default ScrollListPopover
