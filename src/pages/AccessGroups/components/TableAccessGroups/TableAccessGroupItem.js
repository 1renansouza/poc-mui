  
import React, { useState, useRef } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import clsx from 'clsx'

import {
  TableRow,
  TableCell,
  Typography,
} from '@material-ui/core'

import { fade } from '@material-ui/core/styles/colorManipulator'

import {
  Skeleton,
} from '@material-ui/lab'

import {
  Security as SecurityIcon,
} from '@material-ui/icons'

import { ScrollListPopover } from '../'

const useStyles = makeStyles(theme => ({
  detailWrapper: {
    cursor: 'pointer',
  },
  hoverable: {
    borderBottom: '1px dotted',
    paddingBottom: theme.spacing(1),
  },
  wrapper: {
    cursor: 'pointer',
    transition: 'box-shadow 500ms, background 500ms',
    '&:hover': {
      boxShadow: theme.shadows[2],
      backgroundColor: fade(theme.palette.primary.main, 0.05),
    },
  },
  wrapperOpen: {
    boxShadow: theme.shadows[2],
    backgroundColor: fade(theme.palette.primary.main, 0.05),
  },
  textOverflow: {
    maxWidth: '30vw',
  },
}))


const TableAccessGroupItem = ({ loading }) => {
  const classes = useStyles()
  const peopleWrapper = useRef(null)
  const lockWrapper = useRef(null)
  const timeframeWrapper = useRef(null)

  const [openPeopleList, setOpenPeopleList] = useState(false)
  const [openLockList, setOpenLockList] = useState(false)
  const [openTimeframeList, setOpenTimeframeList] = useState(false)

  const onMouseEnterPeople = () => {
    if (openPeopleList === false)
      setOpenPeopleList(true)
  }

  const onMouseLeavePeople = () => {
    if (openPeopleList === true)
      setOpenPeopleList(false)
  }

  const onMouseEnterLock = () => {
    if (openLockList === false)
      setOpenLockList(true)
  }

  const onMouseLeaveLock = () => {
    if (openLockList === true)
      setOpenLockList(false)
  }

  const onMouseEnterTimeframe = () => {
    if (openTimeframeList === false)
      setOpenTimeframeList(true)
  }

  const onMouseLeaveTimeframe = () => {
    if (openTimeframeList === true)
      setOpenTimeframeList(false)
  }

  return (
    <TableRow className={clsx({
      [classes.wrapper]: !loading,
      [classes.wrapperOpen]: !loading && (openPeopleList || openLockList || openTimeframeList)
    })}>
      {
        loading
          ? (
            <TableCell colSpan={5}>
              <Skeleton />
            </TableCell>
          )
          : (
            <>
              <TableCell width='7%' align='center'>
                <SecurityIcon color='primary' />
              </TableCell>
              <TableCell width='48%'>
                <Typography noWrap variant='body2' className={classes.textOverflow}>
                  Access Group Name Access Group Name Access Group Name Access Group Name Access Group Name Access Group Name Access Group Name Access Group Name
                </Typography>
              </TableCell>
              <TableCell
                width='15%'
                ref={peopleWrapper}
                className={classes.detailWrapper}
                onMouseEnter={onMouseEnterPeople}
                onMouseLeave={onMouseLeavePeople}
              >
                <Typography
                  color={openPeopleList ? 'primary' : 'initial'}
                  component='span'
                  variant='body2'
                  className={classes.hoverable}
                >
                  314
                </Typography>
                <ScrollListPopover open={openPeopleList} anchorEl={peopleWrapper.current} />
              </TableCell>
              <TableCell
                width='15%'
                ref={lockWrapper}
                className={classes.detailWrapper}
                onMouseEnter={onMouseEnterLock}
                onMouseLeave={onMouseLeaveLock}
              >
                <Typography
                  color={openLockList ? 'primary' : 'initial'}
                  component='span'
                  variant='body2'
                  className={classes.hoverable}
                >
                  83
                </Typography>
                <ScrollListPopover open={openLockList} anchorEl={lockWrapper.current} />
              </TableCell>
              <TableCell
                width='15%'
                ref={timeframeWrapper}
                className={classes.detailWrapper}
                onMouseEnter={onMouseEnterTimeframe}
                onMouseLeave={onMouseLeaveTimeframe}
              >
                <Typography
                  color={openTimeframeList ? 'primary' : 'initial'}
                  component='span'
                  variant='body2'
                  className={classes.hoverable}
                >
                  Custom
                </Typography>
                <ScrollListPopover open={openTimeframeList} anchorEl={timeframeWrapper.current} />
              </TableCell>
            </>
          )
      }
    </TableRow>
  )
}

export default TableAccessGroupItem
