  
import React, { useState } from 'react'

import {
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
  TableSortLabel,
  Typography,
  Paper,
} from '@material-ui/core'

import TableAccessGroupItem from './TableAccessGroupItem'

const TableAccessGroups = () => {

  const [loading, setLoading] = useState(false)
  const toggleLoading = () => {
    setLoading(!loading)
  }

  return (
    <Paper>
      <Table onClick={toggleLoading}>
        <TableHead>
          <TableRow>
            <TableCell width='7%' />
            <TableCell width='48%'>
              <TableSortLabel active direction='asc'>
                <Typography variant='subtitle2'>
                  Name
                </Typography>
              </TableSortLabel>
            </TableCell>
            <TableCell width='15%'>
              <Typography variant='subtitle2'>
                People
              </Typography>
            </TableCell>
            <TableCell width='15%'>
              <Typography variant='subtitle2'>
                Locks
              </Typography>
            </TableCell>
            <TableCell width='15%'>
              <Typography variant='subtitle2'>
                Timeframe
              </Typography>
            </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {
            Array(20).fill(null).map((item, index) => (
              <TableAccessGroupItem loading={loading} key={index} />
            ))
          }
        </TableBody>
      </Table>
    </Paper>
  )
}

export default TableAccessGroups