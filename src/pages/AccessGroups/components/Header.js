  
import React from 'react'
import clsx from 'clsx'

import {
  Typography,
  Button,
  IconButton,
  Grid,
  FormControl,
  InputLabel,
  OutlinedInput,
} from '@material-ui/core'

import {
  Search as SearchIcon,
  Add as AddIcon,
  Reorder as ReorderIcon,
  Apps as AppsIcon,
} from '@material-ui/icons'

import { makeStyles } from '@material-ui/core/styles'

const useStyle = makeStyles(theme => ({
  root: {
    marginBottom: theme.spacing(2),
  },
  grow: {
    flexGrow: 1,
  },
  paddingTopBottom: {
    paddingTop: theme.spacing(2),
    paddingBottom: theme.spacing(2),
  },
  addButton: {
    marginTop: theme.spacing(2),
  },
  noTransform: {
    textTransform: 'none',
  },
  noMargin: {
    margin: 0,
  },
  inactiveButton: {
    opacity: 0.5,
  },
}))


const Header = ({ activeView, setActiveView, onOpenModal }) => {
  const classes = useStyle()

  return (
    <Grid className={classes.root} container alignItems='center' spacing={3}>
      <Grid item className={classes.grow}>
        <Grid container alignItems='center' spacing={3}>
          <Grid item>
            <Typography variant='h5'>
              Access Groups
            </Typography>
          </Grid>

          <Grid item>
            <Button
              variant='outlined'
              color='primary'
              onClick={onOpenModal}
              className={classes.noTransform}
              startIcon={<AddIcon />}
            >
              Create Access Group
            </Button>
          </Grid>
        </Grid>
      </Grid>

      <Grid item>
        <Grid container alignItems='center' spacing={3}>
          <Grid item>
            <FormControl margin='dense' className={classes.noMargin} variant='outlined'>
              <InputLabel>Search</InputLabel>
              <OutlinedInput
                endAdornment={<SearchIcon color='primary' />}
                labelWidth={50}
              />
            </FormControl>
          </Grid>

          <Grid item>
            <IconButton
              onClick={setActiveView.bind(null, 'grid')}
              className={clsx({[classes.inactiveButton]: activeView === 'list'})}
              color='primary'
            >
              <AppsIcon />
            </IconButton>
            <IconButton
              onClick={setActiveView.bind(null, 'list')}
              className={clsx({[classes.inactiveButton]: activeView === 'grid'})}
              color='primary'
            >
              <ReorderIcon />
            </IconButton>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  )
}

export default Header
