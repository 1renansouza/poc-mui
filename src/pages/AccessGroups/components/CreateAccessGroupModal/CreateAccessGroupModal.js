  
import React, { useState } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import { useSnackbar } from 'notistack'

import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  TextField,
  Button,
  Typography,
} from '@material-ui/core'

import {
  Security as SecurityIcon,
  Lock as LockIcon,
  DateRange as DateRangeIcon,
  PersonAdd as PersonAddIcon,
} from '@material-ui/icons'

import {
  SelectableListPeople,
  SelectableListLock,
  SelectTimeframe,
  AccessGroupSummary,
} from './components'

const useStyles = makeStyles(theme => ({
  header: {
    background: 'rgba(128, 128, 128, 0.15)',
  },
  bigIcon: {
    fontSize: '4rem',
  },
  footer: {
    padding: theme.spacing(2),
  },
  button: {
    boxShadow: theme.shadows[0],
  },
  hiddenOverflow: {
    overflow: 'hidden',
  },
}))

const CreateAccessGroupModal = ({ onClose, open }) => {
  const { enqueueSnackbar } = useSnackbar()
  const [errorMessage, setErrorMessage] = useState('')
  const [accessGroupName, setAccessGroupName] = useState('')
  const [currentStep, setCurrentStep] = useState(0)

  const classes = useStyles()

  const onNextStep = () => {
    if (currentStep === 0) {
      validateAccessGroupName(accessGroupName) && setCurrentStep(currentStep + 1)
    } else {
      setCurrentStep(currentStep + 1)
    }
  }

  const handleClose = () => {
    setCurrentStep(0)
    setAccessGroupName('')
    setErrorMessage('')
    onClose()
  }

  const onSave = () => {
    enqueueSnackbar('Access Group successfully created', { variant: 'success' })
    handleClose()
  }

  const onPreviousStep = () => {
    setCurrentStep(currentStep - 1)
  }

  const handleInputChange = (event) => {
    setAccessGroupName(event.target.value)
    validateAccessGroupName(event.target.value)
  }

  const validateAccessGroupName = (accessGroupName) => {
    if (accessGroupName === '') {
      setErrorMessage('Please, fill in the Access Group Name')
      return false
    } else {
      setErrorMessage('')
      return true
    }
  }

  return (
    <Dialog maxWidth='sm' fullWidth open={open} onClose={handleClose}>
      {
        currentStep === 0 && (
          <FirstStep
            classes={classes}
            handleInputChange={handleInputChange}
            errorMessage={errorMessage}
            onNextStep={onNextStep}
            accessGroupName={accessGroupName}
          />
        )
      }

      {
        currentStep === 1 && (
          <SecondStep
            classes={classes}
            onNextStep={onNextStep}
            onPreviousStep={onPreviousStep}
          />
        )
      }

      {
        currentStep === 2 && (
          <ThirdStep
            classes={classes}
            onNextStep={onNextStep}
            onPreviousStep={onPreviousStep}
          />
        )
      }

      {
        currentStep === 3 && (
          <FourthStep
            classes={classes}
            onNextStep={onNextStep}
            onPreviousStep={onPreviousStep}
          />
        )
      }

      {
        currentStep === 4 && (
          <FifthStep
            classes={classes}
            onSave={onSave}
            onPreviousStep={onPreviousStep}
          />
        )
      }
    </Dialog>
  )
}

const FifthStep = ({
  classes,
  onSave,
  onPreviousStep
}) => (
  <>
    <DialogTitle className={classes.header}>
      <SecurityIcon className={classes.bigIcon} color='primary' />
      <Typography component='p' variant='h6'>
        Access Group Overview
      </Typography>
    </DialogTitle>
    <DialogContent dividers>
      <AccessGroupSummary />
    </DialogContent>
    <DialogActions className={classes.footer}>
    <Button
        onClick={onPreviousStep}
        className={classes.button}
        color='primary'
      >
        Back
      </Button>
      <Button
        onClick={onSave}
        className={classes.button}
        variant='contained'
        color='primary'
      >
        Create Access Group
      </Button>
    </DialogActions>
  </>
)

const FourthStep = ({
  classes,
  onNextStep,
  onPreviousStep
}) => (
  <>
    <DialogTitle className={classes.header}>
      <DateRangeIcon className={classes.bigIcon} color='primary' />
      <Typography component='p' variant='h6'>
        Define a Timeframe
      </Typography>
    </DialogTitle>
    <DialogContent dividers>
      <SelectTimeframe />
    </DialogContent>
    <DialogActions className={classes.footer}>
    <Button
        onClick={onPreviousStep}
        className={classes.button}
        color='primary'
      >
        Back
      </Button>
      <Button
        onClick={onNextStep}
        className={classes.button}
        variant='contained'
        color='primary'
      >
        Next
      </Button>
    </DialogActions>
  </>
)

const ThirdStep = ({
  classes,
  onNextStep,
  onPreviousStep
}) => (
  <>
    <DialogTitle className={classes.header}>
      <LockIcon className={classes.bigIcon} color='primary' />
      <Typography component='p' variant='h6'>
        Select Locks
      </Typography>
    </DialogTitle>
    <DialogContent dividers className={classes.hiddenOverflow}>
      <SelectableListLock />
    </DialogContent>
    <DialogActions className={classes.footer}>
    <Button
        onClick={onPreviousStep}
        className={classes.button}
        color='primary'
      >
        Back
      </Button>
      <Button
        onClick={onNextStep}
        className={classes.button}
        variant='contained'
        color='primary'
      >
        Next
      </Button>
    </DialogActions>
  </>
)

const SecondStep = ({
  classes,
  onNextStep,
  onPreviousStep
}) => (
  <>
    <DialogTitle className={classes.header}>
      <PersonAddIcon className={classes.bigIcon} color='primary' />
      <Typography component='p' variant='h6'>
        Select users
      </Typography>
    </DialogTitle>
    <DialogContent dividers className={classes.hiddenOverflow}>
      <SelectableListPeople />
    </DialogContent>
    <DialogActions className={classes.footer}>
    <Button
        onClick={onPreviousStep}
        className={classes.button}
        color='primary'
      >
        Back
      </Button>
      <Button
        onClick={onNextStep}
        className={classes.button}
        variant='contained'
        color='primary'
      >
        Next
      </Button>
    </DialogActions>
  </>
)


const FirstStep = ({
  accessGroupName,
  classes,
  handleInputChange,
  errorMessage,
  onNextStep
}) => (
  <>
    <DialogTitle className={classes.header}>
      <SecurityIcon className={classes.bigIcon} color='primary' />
      <Typography component='p' variant='h6'>
        Name your access group
      </Typography>
    </DialogTitle>
    <DialogContent dividers>
      <TextField
        autoFocus
        margin='dense'
        label='Access Group Name'
        required
        value={accessGroupName}
        onChange={handleInputChange}
        error={!!errorMessage}
        helperText={errorMessage}
        fullWidth
      />
    </DialogContent>
    <DialogActions className={classes.footer}>
      <Button
        onClick={onNextStep}
        className={classes.button}
        variant='contained'
        color='primary'
      >
        Next
      </Button>
    </DialogActions>
  </>
)


export default CreateAccessGroupModal
