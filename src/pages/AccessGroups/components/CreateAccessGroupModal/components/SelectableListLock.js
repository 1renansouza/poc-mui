import React, { useState } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import clsx from 'clsx'

import {
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  TextField,
  Typography,
  Checkbox,
} from '@material-ui/core'

import {
  Skeleton,
} from '@material-ui/lab'

import {
  Search as SearchIcon,
  Lock as LockIcon,
} from '@material-ui/icons'

const useStyles = makeStyles(theme => ({
  table: {
    padding: 0,
    height: '55vh',
    display: 'inline-block',
    overflow: 'scroll',
  },
  tableHeader: {
    background: 'white',
  },
  textOverflow: {
    maxWidth: '30vw',
  },
  tableRow: {
    cursor: 'pointer',
  },
  tableRowSelected: {
    backgroundColor: 'rgba(0, 0, 0, 0.07)',
  },
  /* list: {
    height: 220,
    overflow: 'scroll',
  }, */
}))

function createData(id, name, selected) {
  return { id, name, selected }
}

const mockPeopleList = [
  createData(0, 'Meeting Room: Rio de Janeiro', false),
  createData(1, 'Meeting Room: California', false),
  createData(2, 'Meeting Room: Pele', false),
  createData(3, 'Kitchen 1st Floor', false),
  createData(4, 'Kitchen 2nd Floor', false),
  createData(5, 'Front Gate', false),
  createData(6, 'Front Door Top', false),
  createData(7, 'Front Door Bottom', false),
  createData(8, 'Storage Room A', false),
  createData(9, 'Storage Room B', false),
  createData(10, 'Storage Room C', false),
  createData(11, 'Gym', false),
  createData(12, 'Parking Lot', false),
]

const SelectableListPeople = () => {
  const classes = useStyles()
  const [peopleList, setPeopleList] = useState(mockPeopleList)
  const [loading, setLoading] = useState(false)
  const [selectedCount, setSelectedCount] = useState(mockPeopleList)

  const handleClick = (item) => {
    const newPeopleList = [...peopleList]
    newPeopleList[item.id].selected = !item.selected

    setPeopleList(newPeopleList)
    setSelectedCount(newPeopleList.filter(item => item.selected).length)
  }

  const toggleAll = () => {
    let newPeopleList = [...peopleList]

    if (selectedCount === peopleList.length) {
      newPeopleList = newPeopleList.map(item => {
        item.selected = false
        return item
      })
    } else {
      newPeopleList = newPeopleList.map(item => {
        item.selected = true
        return item
      })
    }

    setPeopleList(newPeopleList)
    setSelectedCount(newPeopleList.filter(item => item.selected).length)
  }

  const toggleLoading = () => {
    setLoading(!loading)
  }


  const widthCheckbox = '7%'
  const widthIcon = '13%'
  const widthName = '80%'

  return (
    <>
      <TextField
        fullWidth
        label='Search'
        margin='dense'
        InputProps={{
          endAdornment: <SearchIcon fontSize='small' color='primary' />
        }}
      />
      <Table stickyHeader className={classes.table}>
        <TableHead>
          <TableRow>
            <TableCell
              className={classes.tableHeader}
              padding='checkbox'
              component='th'
              width={widthCheckbox}
            >
              <Checkbox
                color='primary'
                indeterminate={selectedCount > 0 && selectedCount < peopleList.length}
                checked={selectedCount === peopleList.length}
                onChange={toggleAll}
              />
            </TableCell>
            <TableCell
              className={classes.tableHeader}
              component='th'
              width={widthIcon}
            />
            <TableCell
              className={classes.tableHeader}
              component='th'
              width={widthName}
              onClick={toggleLoading}
            >
              <Typography variant='subtitle2'>
                Name
              </Typography>
            </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {
            mockPeopleList.map((item, index) => (
              loading
                ? (
                  <TableRow key={index} className={classes.tableRow}>
                    <TableCell width={widthCheckbox}>
                      <Skeleton />
                    </TableCell>
                    <TableCell width={widthIcon}>
                      <Skeleton />
                    </TableCell>
                    <TableCell width={widthName}>
                      <Skeleton />
                    </TableCell>
                  </TableRow>
                )
                : (
                  <TableRow
                    hover
                    className={clsx(classes.tableRow, {[classes.tableRowSelected]: item.selected})}
                    onClick={handleClick.bind(null, item)}
                    key={index}
                  >
                    <TableCell width={widthCheckbox} padding='checkbox'>
                      <Checkbox color='primary' checked={item.selected} />
                    </TableCell>
                    <TableCell align='center' width={widthIcon}>
                      <LockIcon />
                    </TableCell>
                    <TableCell width={widthName}>
                      <Typography
                        noWrap
                        variant='body2'
                        className={classes.textOverflow}
                      >
                        {item.name}
                      </Typography>
                    </TableCell>
                  </TableRow>
                )
            ))
          }
        </TableBody>
      </Table>
    </>
  )
}

export default SelectableListPeople
