import React, { useState } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import clsx from 'clsx'

import {
  Avatar,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  TextField,
  Typography,
  Checkbox,
} from '@material-ui/core'

import {
  Skeleton,
} from '@material-ui/lab'

import {
  Search as SearchIcon,
} from '@material-ui/icons'

const useStyles = makeStyles(theme => ({
  table: {
    padding: 0,
    height: '55vh',
    display: 'inline-block',
    overflow: 'scroll',
  },
  tableHeader: {
    background: 'white',
  },
  textOverflow: {
    maxWidth: '30vw',
  },
  tableRow: {
    cursor: 'pointer',
  },
  tableRowSelected: {
    backgroundColor: 'rgba(0, 0, 0, 0.07)',
  },
  /* list: {
    height: 220,
    overflow: 'scroll',
  }, */
}))

function createData(id, name, tagNumber, selected) {
  return { id, name, tagNumber, selected }
}

const mockPeopleList = [
  createData(0, 'Roxie Oglesby', 123213, false),
  createData(1, 'Kristine Sterner', 123213, false),
  createData(2, 'Talia Mccaster', 123213, false),
  createData(3, 'Hien Konen', 123213, false),
  createData(4, 'Hollis Egnor', 123213, false),
  createData(5, 'Shea Southwell', 123213, false),
  createData(6, 'Carleen Tamez', 123213, false),
  createData(7, 'Annie Hendrick', 123213, false),
  createData(8, 'Lauralee Bernardini', 123213, false),
  createData(9, 'Carman Arnone', 123213, false),
  createData(10, 'Dick Rempe', 123213, false),
  createData(11, 'Antoine Velasques', 123213, false),
  createData(12, 'Magdalene Balderas', 123213, false),
  createData(13, 'Lynelle Fetzer', 123213, false),
  createData(14, 'Flora Mallen', 123213, false),
  createData(15, 'Jake Demyan', 123213, false),
  createData(16, 'Tawny Scheer', 123213, false),
  createData(17, 'Moriah Land', 123213, false),
  createData(18, 'Gilberte Fridley', 123213, false),
  createData(19, 'Prince Lautenschlage', 123213, false),
]

const SelectableListPeople = () => {
  const classes = useStyles()
  const [peopleList, setPeopleList] = useState(mockPeopleList)
  const [loading, setLoading] = useState(false)
  const [selectedCount, setSelectedCount] = useState(mockPeopleList)

  const handleClick = (item) => {
    const newPeopleList = [...peopleList]
    newPeopleList[item.id].selected = !item.selected

    setPeopleList(newPeopleList)
    setSelectedCount(newPeopleList.filter(item => item.selected).length)
  }

  const toggleAll = () => {
    let newPeopleList = [...peopleList]

    if (selectedCount === peopleList.length) {
      newPeopleList = newPeopleList.map(item => {
        item.selected = false
        return item
      })
    } else {
      newPeopleList = newPeopleList.map(item => {
        item.selected = true
        return item
      })
    }

    setPeopleList(newPeopleList)
    setSelectedCount(newPeopleList.filter(item => item.selected).length)
  }

  const toggleLoading = () => {
    setLoading(!loading)
  }


  const widthCheckbox = '7%'
  const widthAvatar = '75px'
  const widthName = '45%'
  const widthTagNumber = '33%'

  return (
    <>
      <TextField
        fullWidth
        label='Search'
        margin='dense'
        InputProps={{
          endAdornment: <SearchIcon fontSize='small' color='primary' />
        }}
      />
      <Table stickyHeader className={classes.table}>
        <TableHead>
          <TableRow>
            <TableCell
              className={classes.tableHeader}
              padding='checkbox'
              component='th'
              width={widthCheckbox}
            >
              <Checkbox
                color='primary'
                indeterminate={selectedCount > 0 && selectedCount < peopleList.length}
                checked={selectedCount === peopleList.length}
                onChange={toggleAll}
              />
            </TableCell>
            <TableCell className={classes.tableHeader} component='th' width={widthAvatar} />
            <TableCell className={classes.tableHeader} component='th' width={widthName}>
              <Typography variant='subtitle2'>
                Name
              </Typography>
            </TableCell>
            <TableCell className={classes.tableHeader} component='th' onClick={toggleLoading} width={widthTagNumber}>
              <Typography variant='subtitle2'>
                Tag Number
              </Typography>
            </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {
            mockPeopleList.map((item, index) => (
              loading
                ? (
                  <TableRow key={index} className={classes.tableRow}>
                    <TableCell width={widthCheckbox}>
                      <Skeleton />
                    </TableCell>
                    <TableCell width={widthAvatar}>
                      <Skeleton />
                    </TableCell>
                    <TableCell width={widthName}>
                      <Skeleton />
                    </TableCell>
                    <TableCell width={widthTagNumber}>
                      <Skeleton />
                    </TableCell>
                  </TableRow>
                )
                : (
                  <TableRow
                    hover
                    className={clsx(classes.tableRow, {[classes.tableRowSelected]: item.selected})}
                    onClick={handleClick.bind(null, item)}
                    key={index}
                  >
                    <TableCell width={widthCheckbox} padding='checkbox'>
                      <Checkbox color='primary' checked={item.selected} />
                    </TableCell>
                    <TableCell align='center' width={widthAvatar}>
                      <Avatar alt='Mock Test'>
                        {item.name.slice(0, 1)}
                      </Avatar>
                    </TableCell>
                    <TableCell width={widthName}>
                      <Typography
                        noWrap
                        variant='body2'
                        className={classes.textOverflow}
                      >
                        {item.name}
                      </Typography>
                    </TableCell>
                    <TableCell width={widthTagNumber}>
                      <Typography variant='body2'>
                        {item.tagNumber}
                      </Typography>
                    </TableCell>
                  </TableRow>
                )
            ))
          }
        </TableBody>
      </Table>
    </>
  )
}

export default SelectableListPeople
