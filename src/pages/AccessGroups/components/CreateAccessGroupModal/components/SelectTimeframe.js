import React from 'react'
import clsx from 'clsx'
import DateFnsUtils from '@date-io/date-fns'

import { makeStyles } from '@material-ui/core/styles'

import {
  Radio,
  RadioGroup,
  FormHelperText,
  FormControlLabel,
  FormControl,
  Typography,
  Checkbox,
  Divider,
  Grid,
  Avatar,
} from '@material-ui/core'

import {
  MuiPickersUtilsProvider,
  KeyboardTimePicker,
  KeyboardDatePicker,
} from '@material-ui/pickers'

const useStyles = makeStyles(theme => ({
  formControl: {
    margin: theme.spacing(0),
    width: '100%',
  },
  backgroundPrimary: {
    backgroundColor: theme.palette.primary.main,
  },
  smallText: {
    fontSize: '12px'
  },
  optionWrapper: {
    padding: theme.spacing(2),
    '&:not:last-child': {
      marginBottom: theme.spacing(2),
    }
  },
  innerDivider: {
    margin: `${theme.spacing(3)}px 0`,
  },
  subtitle: {
    marginBottom: theme.spacing(1),
  },
}))

const SelectTimeframe = () => {
  const classes = useStyles()
  const [value, setValue] = React.useState('custom')

  const handleChange = event => {
    setValue(event.target.value)
  }

  return (
    <div>
      <FormControl component='fieldset' className={classes.formControl}>
        <RadioGroup value={value} onChange={handleChange}>
          <div className={classes.optionWrapper}>
            <FormControlLabel value='always' control={<Radio color='primary' />} label='Always' />
            <FormHelperText>
              Choose always if you want to grant access from now until you revoke it manually
            </FormHelperText>
          </div>
          <Divider />
          <div className={classes.optionWrapper}>
            <FormControlLabel value='custom' control={<Radio color='primary' />} label='Custom Schedule' />
            <FormHelperText>
              Create custom schedules that you can manage access daily and hourly.
            </FormHelperText>
          </div>
        </RadioGroup>
      </FormControl>

      {
        value === 'custom' && 
        <div className={classes.optionWrapper}>
          <CustomSchedule />
        </div>
      }
    </div>
  )
}



const CustomSchedule = () => {
  const [selectedDate, setSelectedDate] = React.useState(null)

  const handleDateChange = date => {
    setSelectedDate(date)
  }

  const classes = useStyles()

  return (
    <>
      <Typography className={classes.subtitle} variant='subtitle2' component='p'>
        Select date(s)
      </Typography>
      <MuiPickersUtilsProvider utils={DateFnsUtils}>
        <Grid container spacing={3}>
          <Grid item xs={12} sm={6}>
            <KeyboardDatePicker
              elevation={2}
              color='primary'
              disableToolbar
              variant='inline'
              format='dd/MM/yyyy'
              label='Start Date'
              value={selectedDate}
              onChange={handleDateChange}
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <KeyboardDatePicker
              elevation={2}
              color='primary'
              variant='inline'
              format='dd/MM/yyyy'
              label='End Date'
              value={selectedDate}
              onChange={handleDateChange}
            />
          </Grid>
        </Grid>
      </MuiPickersUtilsProvider>

      <Divider className={classes.innerDivider} />

      <Typography className={classes.subtitle} variant='subtitle2' component='p'>
        Days - 3 selected
      </Typography>

      {
        ['SU', 'MO', 'TU', 'WE', 'TH', 'FR', 'SA', 'ALL'].map((day, index) => (
          <FormControlLabel key={index}
            control={<Checkbox
              icon={<Avatar className={classes.smallText}>{day}</Avatar>}
              checkedIcon={<Avatar className={clsx(classes.smallText, classes.backgroundPrimary)}>
                {day}
              </Avatar>}
              value='checkedH'
            />}
          />
        ))
      }

      <Divider className={classes.innerDivider} />

      <Typography className={classes.subtitle} variant='subtitle2' component='p'>
        Select timeframe
      </Typography>

      <MuiPickersUtilsProvider utils={DateFnsUtils}>
        <Grid container spacing={3}>
          <Grid item xs={12} sm={5}>
            <KeyboardTimePicker
              label='From'
              value={selectedDate}
              onChange={handleDateChange}
            />
          </Grid>
          <Grid item xs={12} sm={5}>
            <KeyboardTimePicker
              label='To'
              value={selectedDate}
              onChange={handleDateChange}
            />
          </Grid>
          <Grid item xs={12} sm={2}>
            <FormControlLabel
              control={<Checkbox
                icon={<Avatar className={classes.smallText}>24H</Avatar>}
                checkedIcon={<Avatar className={clsx(classes.smallText, classes.backgroundPrimary)}>24H</Avatar>}
                value='checkedH'
              />}
            />
          </Grid>
        </Grid>
      </MuiPickersUtilsProvider>
    </>
  )
}

export default SelectTimeframe
