import React from 'react'
import { makeStyles } from '@material-ui/core/styles'

import {
  Button,
  Divider,
  Grid,
  Typography,
} from '@material-ui/core'

import {
  Edit as EditIcon,
} from '@material-ui/icons'


const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing(3),
  },
}))

export default function AccessGroupSummary() {
  const classes = useStyles()
  return (
    <div className={classes.root}>
        <Grid container alignItems='center'>
          <Grid item xs={5}>
            <Typography variant='caption'>
              Name
            </Typography>
            <Typography variant='subtitle2'>
              Cool Access Group Name
            </Typography>
          </Grid>

          <Grid item xs={7} style={{textAlign: 'right'}}>
            <Button className={classes.hehe} color='primary' startIcon={<EditIcon />}>
              Edit
            </Button>
          </Grid>
        </Grid>
      <Divider style={{margin: '15px 0'}} />
        <Grid container alignItems='center'>
          <Grid item xs={5}>
            <Typography variant='caption'>
              People
            </Typography>
            <Typography variant='subtitle2'>
              18 People
            </Typography>
          </Grid>

          <Grid item xs={7} style={{textAlign: 'right'}}>
            <Button color='primary' startIcon={<EditIcon />}>
              Edit
            </Button>
          </Grid>
        </Grid>
      <Divider style={{margin: '15px 0'}} />
        <Grid container alignItems='center'>
          <Grid item xs={5}>
            <Typography variant='caption'>
              Lock
            </Typography>
            <Typography variant='subtitle2'>
              7 Locks
            </Typography>
          </Grid>

          <Grid item xs={7} style={{textAlign: 'right'}}>
            <Button color='primary' startIcon={<EditIcon />}>
              Edit
            </Button>
          </Grid>
        </Grid>
      <Divider style={{margin: '15px 0'}} />
        <Grid container alignItems='center'>
          <Grid item xs={5}>
            <Typography variant='caption'>
              Timeframe
            </Typography>
            <Typography variant='subtitle2'>
              Always
            </Typography>
          </Grid>

          <Grid item xs={7} style={{textAlign: 'right'}}>
            <Button color='primary' startIcon={<EditIcon />}>
              Edit
            </Button>
          </Grid>
        </Grid>
    </div>
  )
}