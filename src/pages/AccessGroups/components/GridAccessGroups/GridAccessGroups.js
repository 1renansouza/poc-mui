  
import React, { useState } from 'react'

import {
  Grid,
} from '@material-ui/core'

import GridAccessGroupItem from './GridAccessGroupItem'

const GridAccessGroups = () => {

  const [loading, setLoading] = useState(false)
  const toggleLoading = () => {
    setLoading(!loading)
  }

  return (
    <Grid container spacing={2} onClick={toggleLoading}>
      {
        Array(20).fill(null).map((item, index) => (
          <GridAccessGroupItem loading={loading} key={index} />
        ))
      }
    </Grid>
  )
}

export default GridAccessGroups
