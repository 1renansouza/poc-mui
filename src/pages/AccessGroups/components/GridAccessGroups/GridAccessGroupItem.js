  
import React, { useState, useRef } from 'react'
import { makeStyles } from '@material-ui/core/styles'

import {
  Grid,
  Card,
  CardContent,
  Typography,
} from '@material-ui/core'

import {
  Skeleton,
} from '@material-ui/lab'

import {
  Security as SecurityIcon,
} from '@material-ui/icons'

import { ScrollListPopover } from '../'

const useStyles = makeStyles(theme => ({
  cardWrapper: {
    minHeight: 120,
  },
  wrapper: {
    cursor: 'pointer',
    transition: 'box-shadow 500ms, transform 500ms',
    '&:hover': {
      boxShadow: theme.shadows[2],
    },
  },
  content: {
    flex: '1 0 auto',
  },
  cover: {
    width: 151,
  },
  controls: {
    display: 'flex',
    alignItems: 'center',
    paddingLeft: theme.spacing(1),
    paddingBottom: theme.spacing(1),
  },
  playIcon: {
    height: 38,
    width: 38,
  },
  accessGroupDetailListItem: {
    paddingLeft: 0,
    paddingBottom: 0,
  },
  accessGroupDetailListItemIcon: {
    minWidth: 'unset',
    marginRight: theme.spacing(1),
  },
  detailValue: {
    borderBottom: `1px dotted black`,
    display: 'inline-block',
  },
  detailWrapper: {
    marginTop: theme.spacing(1),
  }
}))


const GridAccessGroupItem = ({ loading }) => {
  const classes = useStyles()
  const peopleWrapper = useRef(null)
  const lockWrapper = useRef(null)
  const timeframeWrapper = useRef(null)

  const [openPeopleList, setOpenPeopleList] = useState(false)
  const [openLockList, setOpenLockList] = useState(false)
  const [openTimeframeList, setOpenTimeframeList] = useState(false)

  const onMouseEnterPeople = () => {
    if (openPeopleList === false)
      setOpenPeopleList(true)
  }

  const onMouseLeavePeople = () => {
    if (openPeopleList === true)
      setOpenPeopleList(false)
  }

  const onMouseEnterLock = () => {
    if (openLockList === false)
      setOpenLockList(true)
  }

  const onMouseLeaveLock = () => {
    if (openLockList === true)
      setOpenLockList(false)
  }

  const onMouseEnterTimeframe = () => {
    if (openTimeframeList === false)
      setOpenTimeframeList(true)
  }

  const onMouseLeaveTimeframe = () => {
    if (openTimeframeList === true)
      setOpenTimeframeList(false)
  }

  const handleClick = (event) => {
    event.preventDefault()
    event.stopPropagation()
  }

  return (
    <Grid item xs={12} sm={6} md={4} lg={3} className={classes.cardWrapper}>
      {
        loading
          ? <Skeleton variant='rect' width="100%" height="100%" />
          : (
            <Card className={classes.wrapper} elevation={0} onClick={handleClick}>
              <CardContent className={classes.content}>
                <Grid container spacing={2} alignItems='center'>
                  <Grid item xs={2}>
                    <SecurityIcon color='primary' fontSize='large' />
                  </Grid>
                  <Grid item xs={10}>
                    <Typography title='Full Name :D' noWrap variant='h6' color='primary'>
                      Access Group Name Access Group Name Access Group Name Access Group Name Access Group Name Access Group Name Access Group Name Access Group Name 
                    </Typography>

                    <Grid container>
                      <Grid
                        ref={peopleWrapper}
                        item
                        xs={4}
                        className={classes.detailWrapper}
                        onMouseEnter={onMouseEnterPeople}
                        onMouseLeave={onMouseLeavePeople}
                      >
                        <Typography variant='caption' component='p'>
                          People
                        </Typography>
                        <Typography className={classes.detailValue} variant='body1' color='primary'>
                          314
                        </Typography>
                        <ScrollListPopover open={openPeopleList} anchorEl={peopleWrapper.current} />
                      </Grid>
                      <Grid
                        ref={lockWrapper}
                        item
                        xs={4}
                        className={classes.detailWrapper}
                        onMouseEnter={onMouseEnterLock}
                        onMouseLeave={onMouseLeaveLock}
                      >
                        <Typography variant='caption' component='p'>
                          Locks
                        </Typography>
                        <Typography className={classes.detailValue} variant='body1' color='primary'>
                          83
                        </Typography>
                        <ScrollListPopover open={openLockList} anchorEl={lockWrapper.current} />
                      </Grid>
                      <Grid
                        ref={timeframeWrapper}
                        item
                        xs={4}
                        className={classes.detailWrapper}
                        onMouseEnter={onMouseEnterTimeframe}
                        onMouseLeave={onMouseLeaveTimeframe}
                      >
                        <Typography variant='caption' component='p'>
                          Timeframe
                        </Typography>
                        <Typography className={classes.detailValue} variant='body1' color='primary'>
                          Custom
                        </Typography>
                        <ScrollListPopover open={openTimeframeList} anchorEl={timeframeWrapper.current} />
                      </Grid>
                    </Grid>
                  </Grid>
                </Grid>
              </CardContent>
            </Card>
          )
      }
    </Grid>
  )
}

export default GridAccessGroupItem
