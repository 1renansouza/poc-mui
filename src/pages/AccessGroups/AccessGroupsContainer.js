  
import React from 'react'
import AccessGroups from './AccessGroups'

import Menu from '../../components/MenuKS'

const AccessGroupsContainer = () => {
  return (
    <>
      <Menu />
      <AccessGroups />
    </>
  )
}

export default AccessGroupsContainer
