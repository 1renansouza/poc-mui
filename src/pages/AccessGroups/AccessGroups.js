  
import React, { useState } from 'react'

import {
  Container,
} from '@material-ui/core'

import {
  Header,
  GridAccessGroups,
  TableAccessGroups,
  CreateAccessGroupModal,
} from './components'

const AccessGroups = () => {
  const [activeView, setActiveView] = useState('grid')
  const [openModal, setOpenModal] = useState(false)

  const onModalClose = () => {
    setOpenModal(false)
  }

  const onOpenModal = () => {
    setOpenModal(true)
  }

  return (
    <Container>
      <Header
        activeView={activeView}
        setActiveView={setActiveView}
        onOpenModal={onOpenModal}
      />
      {
        activeView === 'grid'
          ? <GridAccessGroups />
          : <TableAccessGroups />
      }

      <CreateAccessGroupModal open={openModal} onClose={onModalClose} />
    </Container>
  )
}

export default AccessGroups
