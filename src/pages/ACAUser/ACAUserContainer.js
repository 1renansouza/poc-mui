  
import React from 'react'
import ACAUser from './ACAUser'

import Menu from '../../components/MenuACA'

const ACAContainer = () => {
  return (
    <>
      <Menu back />
      <ACAUser />
    </>
  )
}

export default ACAContainer
