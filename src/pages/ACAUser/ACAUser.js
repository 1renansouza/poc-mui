  
import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import clsx from 'clsx'

import {
  Container,
  Grid,
  Typography,
  Button,
  List,
  ListItem,
  ListItemText,
  Divider,
} from '@material-ui/core'

import {
  FiberSmartRecordOutlined as FiberSmartRecordOutlinedIcon,
  ExitToApp as ExitToAppIcon,
  WifiTetheringOutlined as WifiTetheringOutlinedIcon,
} from '@material-ui/icons'


const useStyles = makeStyles(theme => ({
  textButton: {
    fontWeight: 'bolder',
    '& + &': {
      marginLeft: theme.spacing(1),
    },
  },
  button: {
    textTransform: 'none',
    borderRadius: 0,
    marginRight: theme.spacing(1),
  },
  searchInput: {
    border: '1px solid rgba(0, 0, 0, 0.23)',
    padding: `0 ${theme.spacing(2)}px`,
    fontSize: `0.875rem`,
    height: '36px',
    verticalAlign: 'middle',
    marginTop: '1px',
  },
  buttonContained: {
    boxShadow: theme.shadows[0],
    borderRadius: 0,
    color: 'white',
    fontSize: '11px',
    fontWeight: 'bolder',
    verticalAlign: 'bottom',
    height: '36px',
  },
  popoverContent: {
    padding: `${theme.spacing(1)}px ${theme.spacing(2)}px`,
  },
  alignRight: {
    textAlign: 'right',
  },
  divider: {
    margin: `${theme.spacing(1)}px 0`,
  },
  tableHeader: {
    textTransform: 'uppercase',
    fontSize: '11px',
    color: theme.palette.grey[600],
  },
  tableRow: {
    border: '1px solid rgba(224, 224, 224, 1)',
  },
  tableRowInner: {
    border: '1px solid rgba(224, 224, 224, 1)',
  },
  tableCell: {
    padding: '10px 16px',
  },
  officeName: {
    color: theme.palette.secondary.main,
    fontWeight: 'bolder',
  },
  tablePagination: {
    marginTop: theme.spacing(2),
  },
  paginationSelect: {
    paddingRight: theme.spacing(1),
    marginRight: theme.spacing(2),
  },
  pageText: {
    padding: `3px ${theme.spacing(1)}px`,
    cursor: 'pointer',
    transition: 'all 400ms',
    fontWeight: 'bolder',
    opacity: '0.7',
    border: '1px solid transparent',
    borderRadius: '4px',
    '&:hover, &.active': {
      opacity: 1,
      borderColor: theme.palette.primary.main,
    },
    '& + &': {
      marginLeft: theme.spacing(1),
    },
  },
  paginationIcon: {
    verticalAlign: 'middle',
    opacity: '0.75',
    '&:hover': {
      opacity: 1,
    },
  },
  green: {
    color: '#a0cc2e',
    fontWeight: 'bolder',
    verticalAlign: 'middle',
    '& + &': {
      marginLeft: theme.spacing(1),
    },
  },
  greenText: {
    fontSize: '14px',
  },
  userName: {
    marginBottom: theme.spacing(1),
  },
  buttonsWrapper: {
    marginTop: theme.spacing(2),
  },
  smallCircleButton: {
    borderRadius: '15px',
    fontSize: '11px',
    fontWeight: 'bolder',
    borderColor: 'black',
    '& + &': {
      marginLeft: theme.spacing(1),
    },
  },
  smallCircleButtonRed: {
    color: theme.palette.error.main,
    borderColor: theme.palette.error.main,
    '&:hover': {
      backgroundColor: '#ff174417',
    },
  },
  userInfo: {
    padding: theme.spacing(3),
    border: '1px solid rgba(0, 0, 0, 0.3)',
    borderBottom: 'none',
    borderTopLeftRadius: '5px',
    borderTopRightRadius: '5px',
  },
  sideMenu: {
    marginTop: '-7px',
    paddingBottom: '0',
    borderBottom: '1px solid rgba(0, 0, 0, 0.3)',
  },
  sideMenuItem: {
    paddingLeft: theme.spacing(3),
    border: '1px solid transparent',
    borderRightColor: 'rgba(0, 0, 0, 0.3)',
    borderLeftColor: 'rgba(0, 0, 0, 0.3)',
    marginTop: '-2px',
    cursor: 'pointer',
    '&:last-child': {
      borderBottom: 'none',
    },
    '&:hover, &.active': {
      borderTopColor: 'rgba(0, 0, 0, 0.3)',
      borderBottomColor: 'rgba(0, 0, 0, 0.3)',
      borderRightColor: 'transparent',
      '& .MuiTypography-root': {
        opacity: '1',
      },
    },
    '&:hover + &.active': {
      borderTopColor: 'transparent',
    },
    '&.active + &:hover': {
      borderTopColor: 'transparent',
    },
  },
  sideMenuItemText: {
    transition: 'opacity 400ms',
    textTransform: 'uppercase',
    fontSize: '14px',
    fontWeight: 'bolder',
    opacity: '0.65',
  },
  listSpacer: {
    border: '1px solid transparent',
    borderRightColor: 'rgba(0, 0, 0, 0.3)',
    borderLeftColor: 'rgba(0, 0, 0, 0.3)',
    padding: '3px 0',
  },
  sideMenuItemTextWrapper: {
    margin: 0,
  },
  contentLine: {
    padding: theme.spacing(2),
  },
  hoverable: {
    borderBottom: '1px dotted',
    paddingBottom: '3px',
  },
  keyIcon: {
    fontSize: '20px',
    color: '#a0cc2e',
  },
}))


const ACA = () => {
  const classes = useStyles()
  return (
    <Container>
      <Grid container spacing={8}>
        <Grid item xs={3}>
          <SideMenu classes={classes} />
        </Grid>
        <Grid item xs={9}>
          <List>
            <ListItem>
              <ListItemText primary={(
                <Typography variant='subtitle2'>
                  Mike Wazowski has 2 keys:
                </Typography>
              )} />
            </ListItem>
            <Divider />
            <ListItem className={classes.contentLine}>
              <Grid container spacing={2} alignItems='center'>
                <Grid item xs={2}>
                  <Grid spacing={1} container alignItems='center'>
                    <Grid item>
                      <FiberSmartRecordOutlinedIcon className={classes.keyIcon} style={{transform: 'rotate(-90deg)'}} />
                    </Grid>
                    <Grid item>
                      <Typography variant='body2'>
                        000067282
                      </Typography>
                    </Grid>
                  </Grid>
                </Grid>
                <Grid item xs={2}>
                  <Typography variant='body2' className={classes.hoverable} component='span'>
                    TAG
                  </Typography>
                </Grid>
                <Grid item xs={2}>
                  <Typography variant='body2'>
                    Registered
                  </Typography>
                </Grid>
                <Grid item xs={1}>
                  <Typography variant='body2' className={classes.green}>
                    Active
                  </Typography>
                </Grid>
                <Grid item xs={5} className={classes.alignRight}>
                  <Button className={classes.smallCircleButton} size='small' variant='outlined'>
                    Edit Tag
                  </Button>
                  <Button className={clsx(classes.smallCircleButton, classes.smallCircleButtonRed)} size='small' variant='outlined'>
                    Block Tag
                  </Button>
                </Grid>
              </Grid>
            </ListItem>
            <Divider />
            <ListItem className={classes.contentLine}>
              <Grid container spacing={2} alignItems='center'>
                <Grid item xs={2}>
                  <Grid spacing={1} container alignItems='center'>
                    <Grid item>
                      <WifiTetheringOutlinedIcon className={classes.keyIcon} />
                    </Grid>
                    <Grid item>
                      <Typography variant='body2'>
                        Mike's iPhone x
                      </Typography>
                    </Grid>
                  </Grid>
                </Grid>
                <Grid item xs={2}>
                  <Typography variant='body2' className={classes.hoverable} component='span'>
                    Mobile Key
                  </Typography>
                </Grid>
                <Grid item xs={2}>
                  <Typography variant='body2'>
                    Registered
                  </Typography>
                </Grid>
                <Grid item xs={1}>
                  <Typography variant='body2' className={classes.green}>
                    Active
                  </Typography>
                </Grid>
                <Grid item xs={5} className={classes.alignRight}>
                  <Button className={classes.smallCircleButton} size='small' variant='outlined'>
                    Edit Mobile Key
                  </Button>
                  <Button className={clsx(classes.smallCircleButton, classes.smallCircleButtonRed)} size='small' variant='outlined'>
                    Block Mobile Key
                  </Button>
                </Grid>
              </Grid>
            </ListItem>
            <Divider />
          </List>
        </Grid>
      </Grid>
    </Container>
  )
}

const SideMenu = ({ classes }) => {

  return (
    <>
      <div className={classes.userInfo}>
        <Typography variant='h6' component='p' className={classes.userName}>
          Mike Wazowski
        </Typography>
        <div>
          <ExitToAppIcon className={classes.green} />
          <Typography className={clsx(classes.greenText, classes.green)} component='span'>
            ACCESS
          </Typography>
        </div>
        <div className={classes.buttonsWrapper}>
          <Button className={clsx(classes.smallCircleButton, classes.smallCircleButtonRed)} size='small' variant='outlined'>
            Block
          </Button>
          <Button className={classes.smallCircleButton} size='small' variant='outlined'>
            Delete
          </Button>
        </div>
      </div>
      <List className={classes.sideMenu}>
        {['Role & User Info', 'Tag & Mobile Key', 'Access (Office & Space)', 'Offline Access', 'Events'].map((text, index) => (
          <React.Fragment key={index}>
            { index !== 0 && <ListItem className={classes.listSpacer} /> }
            <ListItem className={clsx(classes.sideMenuItem, { active: index === 1})}>
              <ListItemText className={classes.sideMenuItemTextWrapper} primary={(
                <Typography className={classes.sideMenuItemText}>
                  {text}
                </Typography>
              )} />
            </ListItem>
            { index !== 4 && <ListItem className={classes.listSpacer} /> }
          </React.Fragment>
        ))}
      </List>
    </>
  );
}

export default ACA
